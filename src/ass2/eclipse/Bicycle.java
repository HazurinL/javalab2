//Leon Hazurin
//1640570
package ass2.eclipse;

public class Bicycle {
private String manufacturer;
private int numberGears;
private double maxSpeed;

public String getManufacturer(){
	return this.manufacturer;
}
public int getGears() {
	return this.numberGears;
}
public double getSpeed() {
	return this.maxSpeed;
}
public Bicycle(String maker, int gears, double speed) {
	this.manufacturer = maker;
	this.numberGears = gears;
	if (speed > 0) {
		this.maxSpeed = speed;
	}else {
		System.out.println("Speed cant be negative");
	}
}
public String toString() {
	return("manufacturer: " + manufacturer + ", " + "number of gears: " + numberGears + ", " + "MaxSpeed: " + maxSpeed);
}

}